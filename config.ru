require 'bundler/setup'

Bundler.require(:default)

GITHUB = Octokit::Client.new(:access_token => ENV['GITHUB_TOKEN'])

DB = Sequel.connect(:adapter => 'postgres',
                    :host => ENV['POSTGRESQL_SERVICE_HOST'],
                    :port => ENV['POSTGRESQL_SERVICE_PORT'],
                    :database => ENV['OPENSHIFT_APP_NAME'],
                    :user => ENV['OPENSHIFT_POSTGRESQL_DB_USERNAME'],
                    :password => ENV['OPENSHIFT_POSTGRESQL_DB_PASSWORD'])

DB.create_table?(:users) do
  primary_key :id
  String :name
  String :email
  String :github
end

class Application < Sinatra::Base

  get '/' do
    'Nothing special!'
  end

  post '/' do
    body = params['body-plain']
    name = /^Name: (.+)$/.match(body)[1].strip
    email = /^Email: (.+)$/.match(body)[1].strip
    gh = /^Github Username: (.+)$/.match(body)[1].strip
    DB[:users].insert(:name => name, :email => email, :github => gh)
    puts GITHUB.add_team_membership(ENV['GITHUB_TEAM_ID'], gh, :role => 'member').inspect
  end

end

run Application
